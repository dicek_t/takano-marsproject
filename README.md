# MarsProject
このディレクトリはマーズプロジェクトの為に作成されました。

## CarWheel
車輪制御プログラムが入っているディレクトリです。
- 内容物

1. Controller.py
2. MotorControl.py
3. MotorTest.py
4. `__pycache__`
5. googlehome

### Controller.py
ローバーをコントロールするプログラムです。
起動すると簡単な使い方を表示します。

main functionが定義されています。

### MotorControl.py
ローバーに接続されているモーターを初期化(セットアップ)、動作の定義をするライブラリです。

### MotorTest.py
ローバーに接続されているモーターの動作テストを目的として開発されたプログラムです。
MotorControl.pyが完成した今、使用されていません

### `__pycache__/`
これはディレクトリです。
中には`Controller.pyc`と`MotorControl.pyc`が入っています。
この２つは`Controller.py`と`MotorControl.py`をコンパイルした物が入っています。

### googlehome
これはディレクトリです。
`firebase`ディレクトリ`firebase-debug.log`が入っています。
`firebase`ディレクトリにはfirebaseの操作するためにインストールしたfirebase-toolsの中身が入っています。

## index.js
firebaseで動くwebAPIのプログラムです。firebase-toolsをインストールすると作られます。

### python
これはディレクトリです。
`forward.py`と`back.py`、`stop.py`、`MotorControl.py`が入っています。
これらはroverのタイヤを制御するプログラムです。



## RobotArm
ローバーに乗っているロボットアームを制御するプログラムです
~~動作していません。~~

- 内容物

1. Arduino
2. PytonCode

## file
その他のファイルが入っています。

