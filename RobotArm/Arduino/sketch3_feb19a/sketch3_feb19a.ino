// This is source code to control the arm robot with available register.

#include<Servo.h>
Servo servo,servo2,servo3,servo4;
 
void setup(){
 
  servo.attach(3);
  servo2.attach(5);
  servo3.attach(6);
  servo4.attach(9);
 
  servo.write(0);
  servo2.write(0);
  servo3.write(0);
  servo4.write(0);
 
}
 
void loop(){
  int val=analogRead(A0);
  int val2=analogRead(A1);
  int val3=analogRead(A2);
  int val4=analogRead(A3);
 
 
 
  int deg=map(val,0,1023,0,123);
  int deg2=map(val2,0,1023,0,175);
  int deg3=map(val3,0,1023,0,175);
  int deg4=map(val4,0,1023,0,360);
 
  servo.write(deg);
  servo2.write(deg2);
  servo3.write(deg3);
  servo4.write(deg4);
 
 
  delay(10);
}
