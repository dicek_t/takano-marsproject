

// include the Servo library
#include <Servo.h>

Servo myServo1; 
Servo myServo2;
Servo myServo3;
Servo myServo4;


int potVal1;  // variable to read the value from the analog pin
int potVal2;
int potVal3;
int potVal4;
int angle1;   // variable to hold the angle for the servo motor
int angle2;
int angle3;
int angle4;

void setup() {
  myServo1.attach(9); // attaches the servo on pin 9 to the servo object
  myServo2.attach(10);
  myServo3.attach(11);
  myServo4.attach(12);
  Serial.begin(9600); // open a serial connection to your computer
}

void loop() {
  potVal1 = analogRead(A0); // read the value of the potentiometer
  potVal2 = analogRead(A1);
  potVal3 = analogRead(A2);
  potVal4 = analogRead(A3);
  // print out the value to the Serial Monitor
  Serial.print("potVal: ");
  Serial.print(potVal1);

  // scale the numbers from the pot
  angle1 = map(potVal1, 0, 1023, 0, 179);
  angle2 = map(potVal2, 0, 1023, 0, 179);
  angle3 = map(potVal3, 0, 1023, 0, 179);
  angle4 = map(potVal4, 0, 1023, 0, 179);

  // print out the angle for the servo motor
  Serial.print(", angle: ");
  Serial.println(angle1);

  // set the servo position
  myServo1.write(angle1);
  myServo2.write(angle2);
  myServo3.write(angle3);
  myServo4.write(angle4);

  // wait for the servo to get there
  delay(15);
}
