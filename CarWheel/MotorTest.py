#!/usr/bin/python3
#! -*- coding: utf-8 -*-

# import 
import pigpio
import time

# GPIO setup
pi = pigpio.pi()
## Use Pin
RightPwmPin = 12
RightTruePin = 20 
RightBackPin = 21
## function
def gpioSetup():
    # Right Pin set
    pi.set_mode(RightPwmPin, pigpio.OUTPUT) # PWM pin set
    pi.set_mode(RightTruePin, pigpio.OUTPUT)
    pi.set_mode(RightBackPin, pigpio.OUTPUT)

# Rotation function
## RightMotor Set function
### Advance
def trueRightRotation():
    pi.write(RightTruePin, 1)
def reverseRightRotation():
    pi.write(RightBackPin, 1) 
### Stop
def trueRightRotationStop():
    pi.write(RightTruePin, 0)
def reverseRightRotationStop():
    pi.write(RightBackPin, 0) 

# main function
def main():
    gpioSetup()
    for count in range(5):
        trueRightRotation()
        print("右回転")
        time.sleep(1)

        trueRightRotationStop()
        print("ストップ")
        time.sleep(1)

        reverseRightRotation()
        print("左回転")
        time.sleep(1)

        reverseRightRotationStop()
        print("ストップ")
        time.sleep(1)
    


if __name__ == "__main__":
    main()

