#!/usr/bin/python3
# -*- coding: utf-8 -*-

# import

import getch
import MotorControl


def main():
    MotorControl.gpioSetup()
    print("=====操作方法=====\nwキー　＝　前進\naキー　＝　左回転\nsキー　＝　バック\ndキー　＝　右回転\n Spaceキー　＝　ストップ \n==================")
    print("入力受付開始！")
    while True:
        if 'w' == getch.getch():
            MotorControl.advance()
            print("w")
        elif 's' == getch.getch():
            MotorControl.back()
            print("s")
        elif 'a' == getch.getch():
            MotorControl.rightPivotTurn()
            print("a")
        elif 'd' == getch.getch():
            MotorControl.leftPivotTurn()
            print("d")
        else :
            MotorControl.stop()
            print("Stop")

if __name__ == "__main__":
    main()

